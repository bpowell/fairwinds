# Go Application
This is a very simple bare bones go application that creates a web server that listens on port 8080. To access the application `http://<PUBLIC-IP>:8080/`

## Building
### Docker
To build the docker image, follow the standard docker workflow. `docker build . -t org/appname:tag`

### Locally
Go version 1.16 was used to create this project. To build run `go build`. This will output a binary in the same directory named app.

## Docker Compose
To help deploy the application to EC2 with docker, there is a [docker-compose.yml](docker-compose.yml) file that will be used to quickly deploy the application. Using docker compose saves the step of having to build a docker image and push it up to a docker registry and then having to worry about pulling that down and getting the docker run configuration correct.
