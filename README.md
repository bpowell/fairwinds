# Tech Challenge
To get started, just run the `start.sh` script! It will call out to terraform to create our EC2 instance and then will deploy the application to it. After that is completed it will output the public IP that can be used to access the application.

## Tools Needed
1. terraform
2. go
3. aws cli (only needed if you do not want to manual create aws configuration

### Terraform
For more information please see the [README](terraform/README.md) in the terraform directory

### Go
For more information please see the [README](app/README.md) in the app directory
