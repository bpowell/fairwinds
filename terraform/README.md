# Terraform Configuration
This contains all of the terraform to connect to AWS to create our EC2 instance. Terraform 0.14.7 was used to create everything.

## Initial Setup
The [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html#cliv2-linux-prereq) tool can be used to configure access for terraform. Run `aws configure` to configure access. You will need access to the AccessKeyId and SecretAccessKey to be able to talk to AWS. The region that is used is `eu-west-1`. If you would like to connect to the EC2 instance via SSH, make sure to create a SSH key pair in the [AWS console](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#KeyPairs:) and change the key name to be what key pair that was created in the console.

## Accessing
SSH is accessible to any IP address. In an ideal world, this would be either locked down based on an internal only CIDR range or not allowed at all.

The other port that is exposed via ingress is 8080 for users to talk to the application that will be deployed to EC2.

## User Data
The EC2 instance gets configured by running the provided user data script. This script will install all of the tools needed for the application to run. It will install git, docker, and docker-compose for us. After those dependencies are installed, the script will clone our repository for us. Once the repo is cloned, it will use docker-compose to build the docker image and then run the application listening on port 8080.
