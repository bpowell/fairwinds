#!/usr/bin/env bash
set -xe

cd terraform
terrafrom apply -auto-approve
echo "Public IP of EC2 instance"
terraform state show -state=terraform.tfstate aws_instance.web | grep "public_ip[^_]"
